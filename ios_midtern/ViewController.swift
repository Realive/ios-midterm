//
//  ViewController.swift
//  ios_midtern
//
//  Created by student on 2020/5/4.
//  Copyright © 2020年 realive. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    enum mode {
        case add
        case minus
        case mult
        case div
        case normal
    }
    
    @IBOutlet weak var calculateLabel: UILabel!
    var calculateString: String = "" {
        didSet {
            calculateLabel.text = calculateString
        }
    }
    
    @IBOutlet weak var historyLabel: UILabel!
    var historyString: String = "" {
        didSet {
            historyLabel.text = historyString
        }
    }
    var haveDot: Bool = false
    var waitForEqualButton: Bool = false
    var stageString: String = ""
    var cleanMode:Bool = false
    var calMode: mode = mode.normal

    @IBOutlet var numberCollection: [UIButton]!
    let numberChar = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."]
    
    @IBAction func numberButtonAction(_ sender: UIButton) {
        if (cleanMode) {
            calculateString = ""
            cleanMode = false
            haveDot = false
        }
        if let number = numberCollection.index(of: sender) {
            if (calculateString.count < 10) {
                if (calculateString == "0") {
                    if (numberChar[number] != ".") {
                        calculateString = numberChar[number]
                    } else {
                        calculateString.append(numberChar[number])
                        haveDot = true
                    }
                } else {
                    if (numberChar[number] == ".") {
                        if (!haveDot) {
                            calculateString.append(numberChar[number])
                            haveDot = true
                        }
                    } else {
                        calculateString.append(numberChar[number])
                    }
                }
            }
        }
    }
    @IBAction func equalButtonAction(_ sender: Any) {
        calculateString = cleanCalculateString(str: calculateString)
        var val: Float = 0.0
        switch (calMode) {
        case mode.add:
            historyString = ("\(stageString)+\(calculateString)=")
            val = stageString.floatValue + calculateString.floatValue
            break;
        case mode.minus:
            historyString = ("\(stageString)-\(calculateString)=")
            val = stageString.floatValue - calculateString.floatValue
            break;
        case mode.mult:
            historyString = ("\(stageString)×\(calculateString)=")
            val = stageString.floatValue * calculateString.floatValue
            break;
        case mode.div:
            if (calculateString == "0") {
                val = 0
            } else {
                historyString = ("\(stageString)÷\(calculateString)=")
                val = stageString.floatValue / calculateString.floatValue
                break;
            }
        default:
            break;
        }
        calMode = mode.normal
        calculateString = cleanCalculateString(str:"\(val)")
        cleanMode = true
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        stageString = cleanCalculateString(str:calculateString)
        historyString = "\(stageString)+"
        calculateString = ""
        waitForEqualButton = true
        haveDot = false
        calMode = mode.add
    }
    @IBAction func minusButtonAction(_ sender: Any) {
        stageString = cleanCalculateString(str:calculateString)
        historyString = "\(stageString)-"
        calculateString = ""
        waitForEqualButton = true
        haveDot = false
        calMode = mode.minus
    }
    @IBAction func multButtonAction(_ sender: Any) {
        stageString = cleanCalculateString(str:calculateString)
        historyString = "\(stageString)×"
        calculateString = ""
        waitForEqualButton = true
        haveDot = false
        calMode = mode.mult
    }
    @IBAction func divButtonAction(_ sender: Any) {
        stageString = cleanCalculateString(str:calculateString)
        historyString = "\(stageString)÷"
        calculateString = ""
        waitForEqualButton = true
        haveDot = false
        calMode = mode.div
    }
    @IBAction func perButtonAction(_ sender: Any) {
        let val = calculateString.floatValue
        calculateString = ("\(val/100.0)")
        historyString = "0"
    }
    
    @IBAction func allClearButtonAction(_ sender: Any) {
        calculateString = "0"
        historyString = ""
        stageString = ""
        haveDot = false
        calMode = mode.normal
        waitForEqualButton = false
    }
    
    @IBAction func setMinusButtonAction(_ sender: Any) {
        if ((calculateString.count > 0) && (calculateString != "0")) {
            calculateString = cleanCalculateString(str: calculateString)
            historyString = "±(\(calculateString))="
            if (calculateString.hasPrefix("-")) {
                _ = calculateString.removeFirst()
            } else {
                calculateString.insert("-", at: calculateString.startIndex)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calculateString = "0"
        historyString = ""
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cleanCalculateString(str: String) -> String {
        var word = str
        if (word.last == "0"){
            if (word.contains(".")) {
                _ = word.removeLast()
                return(cleanCalculateString(str:word))
            } else {
            return word
            }
        } else if (word.last == ".") {
            _ = word.removeLast()
            haveDot = false
            return word
        } else {
            return word
        }
    }
}

extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}
